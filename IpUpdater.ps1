

[string]$global:dir_loc = (Split-Path -Parent -Path $MyInvocation.MyCommand.Definition)+"\"

[string]$global:job_filename = $MyInvocation.MyCommand.Name

[string]$global:xml_fn = "cpiplist.xml";

[string]$global:date_time_format = ‘dd-MM-yyyy hh:mm tt’;

$global:dbconn = New-Object System.Data.SQLClient.SQLConnection


function db_query{
    $xml_file = Get-Content ($dir_loc+$xml_fn) |  select -Skip 1  
    $dbconn.Open()
    $linq = "DECLARE @xml XML                                                   "+
            "select @xml = N'"+$xml_file+"'                                     "+
            "                                                                   "+
            ";with [current] as (                                               "+
            "	SELECT                                                          "+
            "	T.C.value('(cpcode)[1]', 'nvarchar(20)') AS cpcode              "+
            "	,T.C.value('(ip)[1]', 'nvarchar(20)') AS ip                     "+
            "	,T.C.value('(lastseen)[1]', 'nvarchar(20)') AS lastseen         "+
            "	FROM @xml.nodes('carparklist/carpark') T(C)                     "+
            "),                                                                 "+
            "                                                                   "+
            "[new] as (                                                         "+
            "	SELECT                                                          "+
            "	CAST(CPCODE as varchar) as cpcode,                              "+
            "	CAST(CPIP as varchar)as ip,                                     "+
            "	'' AS lastseen                                                 "+
            "	FROM [CarParkCentral].[dbo].[CarParkVPNList] B                  "+
            "	WHERE   ACTIVE = '1' AND B.CPCODE NOT IN (                      "+
            "		SELECT                                                      "+
            "		T.C.value('(cpcode)[1]', 'nvarchar(20)') AS cpcode          "+
            "		FROM @xml.nodes('carparklist/carpark') T(C)                 "+
            "	)                                                               "+
            ")                                                                  "+
            "	                                                                "+
            "SELECT                                                             "+
            "CAST([final].CPCODE as varchar) as cpcode,                         "+
            "CAST([final].ip as varchar)as ip,                                  "+
            "CAST([final].lastseen as varchar) as lastseen                      "+
            "FROM (                                                             "+
            "	SELECT * FROM (                                                 "+
            "		SELECT * FROM [current]                                     "+
            "		UNION ALL                                                   "+
            "		SELECT * FROM [new]                                         "+
            "	)[all]                                                          "+
            ")[final]                                                           "+
            "ORDER BY CPCODE                                                    "+
            "FOR XML PATH('carpark'), ROOT('carparklist')"
            
    $command = $dbconn.CreateCommand()
    $command.CommandText = $linq
    $result = $command.ExecuteReader()
    
    
    $string_res=""
 
    while ($result.Read()) {
        $string_res+= $result.GetValue(0)
    }
    $dbconn.Close()   
    return $string_res
    
}


$dbconn.ConnectionString = "server=10.10.0.72;Initial Catalog=CarParkCentral;uid=sa;pwd=sa123!@# ;Integrated Security=False;"

$xml_string = db_query
$r = new-object System.Xml.XmlTextReader(new-object System.IO.StringReader('<?xml version="1.0"?>'+$xml_string))
$sw = new-object System.IO.StringWriter
$w = new-object System.Xml.XmlTextWriter($sw)
$w.Formatting = [System.Xml.Formatting]::Indented
do { $w.WriteNode($r, $false) } while ($r.Read())
$w.Close()
$r.Close()

$sw.ToString()  | Out-File ($dir_loc+$xml_fn)
 
 
#TEST THE NEW CONFIGURED IP LIST

[xml]$XmlDocument = New-Object XML
$XmlDocument.Load($dir_loc+$xml_fn)
$carparklist = $XmlDocument.carparklist
foreach($carpark in $carparklist.carpark){  
     [string] $lastseen = $carpark.lastseen
     [string] $datetime =  (get-date).ToString($date_time_format)
     
    if([string]::IsNullOrEmpty($lastseen)){
        $carpark.lastseen = $datetime
        $XmlDocument.save($dir_loc+$xml_fn)
    }
}