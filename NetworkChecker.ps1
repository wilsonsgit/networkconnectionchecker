################################################
################################################
#######     CAR PARK NETWORK CHECKER     #######        
#######           VERSION 1.7            #######
#######          10 March 2016           #######
################################################
################################################


# Start of script
# NetworkChecker.ps1
# 10 March 2016
# 
# Purpose:
# Powershell script to find out unreachable PMS server
# and send notification email
#
# Requirements:
# IP list in xml format (see cpiplist.xml)
# Recipients for the email (see recipient.xml)
#
# Note: This script must run on the server's Task Scheduler
#


# Note: Uncomment next line for the first run
#Set-ExecutionPolicy RemoteSigned


#Variables to be changed
# Min minutes before email trigger
[string]$global:fail_send_min = "90"


# Maximum minutes
[string]$global:fail_outs_send_min = "360"

$font = "<font size=`"3`" face=`"Calibri`">"

################################################
# Should require no change below this line
# (Except message body)
################################################

[string]$global:dir_loc = (Split-Path -Parent -Path $MyInvocation.MyCommand.Definition)+"\"

$global:dbconn = New-Object System.Data.SQLClient.SQLConnection

[string]$global:job_filename = $MyInvocation.MyCommand.Name

[string]$global:xml_fn = "cpiplist.xml";

[string]$global:email_filename = "recipient.xml"

[string]$global:xml_verbose_log = "Verbose.txt" 

[string]$global:xml_debug_log = "Debug.txt"

[string]$global:date_time_format = ‘dd-MM-yyyy hh:mm tt’;

[string]$global:log_start_time = (get-date).ToString($date_time_format)

[string]$global:log_end_time = (get-date).ToString($date_time_format)

[string]$global:log_string = ""

[string]$global:log_string_email = ""

[xml]$global:XmlDocument = New-Object XML

[int]$global:error_counter=0;


function save_xml_errdetails{
    param ($fname)

    [xml]$xml_log = New-Object XML
    $xml_log.Load($dir_loc+"Error\"+$fname+".xml")
    
    $unreachable_node =   $xml_log.unreachablecp
     
    $carparklist_node = $unreachable_node.carparklist
  
   
    foreach($carpark in $carparklist_node.carpark){
         $dbconn.Open()
        $cpcode_child = $carpark.cpcode
        $ip_child= $carpark.ip
        $lastseen_child = $carpark.lastseen
             
        $linq = "INSERT INTO [CarParkCentral].[dbo].[CP_CONN_STAT_E]	                                          "+
                "           ([ERROR_CODE]                                                                         "+
                "           ,[CPCODE]                                                                             "+
                "           ,[IP]                                                                                 "+
                "           ,[LASTSEEN])                                                                          "+
                "   VALUES  ('"+$fname+"','"+$cpcode_child+"','"+$ip_child +"','"+$lastseen_child+"')             "
                
        $command = $dbconn.CreateCommand()
        $command.CommandText = $linq
        $command.ExecuteReader()
        $dbconn.Close()   
     
    }
 
}

function addverboselog{
    [string]$loc = $dir_loc+$xml_verbose_log;
    [string]$logcontent = "Job From:`t"+$job_filename+"`r`nStarted At:`t"+$log_start_time+"`r`nEnded At:`t"+$log_end_time 
    [string]$err_filename
    [string]$err_filename = ""
    if($error_counter -gt 0){
        $err_filename = adderrfile
     
        [string]$err_as_str = $error_counter -as [string]
        [string]$log_remarks = "Warning! ["+$err_as_str+"] Unreachable Server(s), See 'Error\"+$err_filename+".xml"+"'"
        $logcontent = $logcontent+"`r`nRemarks:`t"+$log_remarks;  
    }else{
          $logcontent = $logcontent+"`r`nRemarks:`t[OK] All server are up and running.";  
    }
    $logcontent = $logcontent+"`r`n-------------------------------------------------------------------------------------"; 
    Add-Content $loc $logcontent
    
    $dbconn.Open()
    $linq = "INSERT INTO [CarParkCentral].[dbo].[CP_CONN_STAT_V]	                                          "+
            "           ([JOB_FROM]                                                                           "+
            "           ,[START_TIME]                                                                         "+
            "           ,[END_TIME]                                                                           "+
            "           ,[ERROR_CODE])                                                                        "+
            "   VALUES  ('"+$job_filename+"','"+$log_start_time+"','"+$log_end_time +"','"+$err_filename+"')   "
       

            
    $command = $dbconn.CreateCommand()
    $command.CommandText = $linq
    $command.ExecuteReader()
    $dbconn.Close()   

    if($error_counter -gt 0){
      save_xml_errdetails $err_filename
    }
}

function checkandcreatefile{
    param([string]$file )
    [string]$loc = $dir_loc+$file;
    if ( -Not (Test-Path $loc))
    {
        New-Item -Path $loc -ItemType file | out-null
    }
}

function adderrfile{
    [string]$filename = Get-Date -format G
    $filename = $filename -replace "/","" -replace ":","" -replace " ",""
    $filename = "Error\"+$filename+".xml"
    emailManager
    checkandcreatefile $filename
    seterrfilecontent $filename
    return $filename
}

function seterrfilecontent{
    param([string] $filename)
    [string]$loc = $dir_loc+$filename;
    [string]$xml_header = '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?><unreachablecp>'
    [string]$logcontent = "<taskdetails><owner>"+$job_filename+"</owner><starttime>"+$log_start_time+"</starttime><endtime>"+$log_end_time +"</endtime></taskdetails>"
    $log_string = $xml_header+$logcontent+"<carparklist>"+$log_string+"</carparklist></unreachablecp>"
    $r = new-object System.Xml.XmlTextReader(new-object System.IO.StringReader($log_string))
    $sw = new-object System.IO.StringWriter
    $w = new-object System.Xml.XmlTextWriter($sw)
    $w.Formatting = [System.Xml.Formatting]::Indented
    do { $w.WriteNode($r, $false) } while ($r.Read())
    $w.Close()
    $r.Close()
    Add-Content $loc $sw.ToString()
}

function emailManager{
   [xml]$emailXml = New-Object XML
   $emailXml.Load($dir_loc+$email_filename)
   
   $emailrecipients = $emailXml.emailrecipients;
   $receivers = $emailrecipients.receiver
         
   $email_sender_temp = $emailrecipients.sender.name
   [string]$email_sender = $emailrecipients.sender.name+"<"+$emailrecipients.sender.email+">"
   [string]$email_title = "[ALERT] CP LINK DOWN"   
   [string]$email_body = "Our server has lost connectivity to the car parks below. Please check site PC.<br/><br/><table style=`"text-align:center`" BORDER=1 WIDTH=`"70%`"><thead><tr><th>Car Park Code</th><th>IP Address</th><th>Active Last</th></tr></thead>"+$log_string_email+"</table><br/><br/>Please ignore this, if issue has been reported. <br/><br/><br/><hr/>This is an auto-generated message."
 

   
   $to = @() 
   foreach($receiver in $receivers.user){  
      $to += $receiver.name+"<"+$receiver.email+">"
   }
   
   $tocc = @()
   $cclist = $receivers.cc;
   foreach($cc in $cclist.user){  
      $tocc += $cc.name+"<"+$cc.email+">"
   }
   
   
   $PSEmailServer = "SBRPARKEXCH01.wilson.com"
   Send-Mailmessage -To $to -From $email_sender -CC $tocc -Subject $email_title -Body ($font+$email_body) -BodyAsHtml
}

function errorEmail{
   param([string]$dump1, [string]$dump2)
   
   [xml]$emailXml = New-Object XML

   $emailXml.Load($dir_loc+$email_filename)
   $emailrecipients = $emailXml.emailrecipients;
   $receivers = $emailrecipients.errorreceiver
   
   $email_sender_temp = $emailrecipients.sender.name
   [string]$email_sender = $emailrecipients.sender.name+"<"+$emailrecipients.sender.email+">"
   [string]$email_title = "WARNING: CAR PAR MONITOR TASK FAILED"   
   [string]$email_body = $dump1+"`r`n"+$dump2+"<br/><br/>"+"This is an auto-generated message."

   
   $to = @() 
   foreach($receiver in $receivers.user){  
      $to += $receiver.name+"<"+$receiver.email+">"
   }
   $PSEmailServer = "SBRPARKEXCH01.wilson.com"
   Send-Mailmessage -To  $to -From $email_sender  -Subject $email_title -Body ($font+$email_body) -BodyAsHtml 
}

function debugError{
    param([string]$dump1, [string]$dump2)
    checkandcreatefile $xml_debug_log
    
    [string]$loc = $dir_loc+$xml_debug_log;  
    [string]$body= (Get-Date -format g)+"`r`n`r`n"+$dump1+"`r`n"+$dump2+"`r`n`r`n"+"-------------------------------------------------------------------------------------"
    Add-Content $loc $body
}

function check_span_for_email{
    param($seen_date)
    $current_time = (get-date).ToString($date_time_format) 
    $seen_date = [datetime]::ParseExact($seen_date,$date_time_format,[System.Globalization.CultureInfo]::CurrentCulture)
    
    $datediff = NEW-TIMESPAN –Start $seen_date –End $current_time
    $datediff.TotalMinutes
    return ( ($datediff.TotalMinutes -ge $fail_send_min) -and ($datediff.TotalMinutes -le $fail_outs_send_min) )
}


$dbconn.ConnectionString = "server=10.10.0.72;Initial Catalog=CarParkCentral;uid=sa;pwd=sa123!@# ;Integrated Security=False;"

try{
    checkandcreatefile $xml_verbose_log

    $XmlDocument.Load($dir_loc+$xml_fn);
    $carparklist = $XmlDocument.carparklist;
    foreach($carpark in $carparklist.carpark){  
        [string] $ip = $carpark.ip
        [string] $cpcode = $carpark.cpcode
        [string] $lastseen = $carpark.lastseen
        [string] $datetime =  (get-date).ToString($date_time_format)
        
        $pwout ="Sending packets to  [ "+$cpcode+" ]`t"+ $ip 
        Write-Host $pwout -nonewline
        if( -Not [string]::IsNullOrEmpty($ip)){
            [string]$result = (Test-Connection $ip -Count 2 -Quiet ) | Out-String
            if($result -match 'False'){
               
                if( (check_span_for_email $lastseen) -match 'True'){
                    $error_counter =  $error_counter + 1
                    $log_string_email=$log_string_email+"<tr><td>"+$cpcode+"</td><td><a href=`"http:\\"+$ip+"`">"+$ip+"</a></td><td>"+$lastseen+"</td></tr>"    
                     $log_string=$log_string+"<carpark><cpcode>"+$cpcode+"</cpcode><ip>"+$ip+"</ip><lastseen>"+$lastseen+"</lastseen></carpark>"
                }
                
                Write-Host "`t[ Failed  ]"
            }else{
                Write-Host "`t[ Success ]"
                $carpark.lastseen = $datetime
            }
        }
    }
    $log_end_time = (get-date).ToString($date_time_format)
    $XmlDocument.save($dir_loc+$xml_fn)
    addverboselog
}catch {
    $ErrorMessage = $_.Exception.Message
    $FailedItem = $_.Exception.ItemName
    errorEmail $ErrorMessage $FailedItem
    debugError $ErrorMessage $FailedItem
}
